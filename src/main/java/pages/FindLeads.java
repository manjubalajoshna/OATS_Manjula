package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	public MergeLeadsPage clickRowOne() {
		WebElement eleRowOne = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(eleRowOne);
		return new MergeLeadsPage();
	}
	
	public MergeLeadsPage clickRowTwo() {
		WebElement eleRowTwo = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]");
		click(eleRowTwo);
		return new MergeLeadsPage();
	}
	

}
