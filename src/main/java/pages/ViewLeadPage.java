package pages;

import org.openqa.selenium.WebElement;

public class ViewLeadPage extends FindLeadsPage {
	
//Edit Lead	

	public OpenTapsCrmPage clickEdit() {
		WebElement eleEditclick = locateElement("linktext", "Edit");
		click( eleEditclick);
		return new OpenTapsCrmPage();
	}

	
	public ViewLeadPage verifyExactText() {
		
		
		WebElement verifyedit = locateElement("xpath", "//span[contains(text(),'TCS')]");
		verifyExactText(verifyedit, "TCS");
		
		return this;
	}

//Delete Lead
	
	public MyLeadsPage clickDelete() {
		WebElement eleDeleteclick = locateElement("xpath", "//a[@class='subMenuButtonDangerous']");
		click( eleDeleteclick);
		return new MyLeadsPage();
	}

	//Merge Lead
	
	public FindLeadsPage clickFindLeads() {
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click( eleFindLeads);
		return new FindLeadsPage();
	}
	
	//Duplicate Lead
	
	public DuplicateLeadPage clickDuplicateLead() {
		WebElement eleclickDuplicateLead = locateElement("xpath", "//a[text()='Duplicate Lead']");
		click( eleclickDuplicateLead);
		return new DuplicateLeadPage();
	}
	
	
}
