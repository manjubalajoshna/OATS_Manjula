package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods {
	
	public FindLeads clickFromLead() throws InterruptedException {
		WebElement eleFromLead = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFromLead);
		Thread.sleep(1000);
		return new FindLeads();
	}
	
	public FindLeads clickToLead() throws InterruptedException {
		WebElement eleToLead = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleToLead);
		Thread.sleep(1000);
		return new FindLeads();
	}
	
	public ViewLeadPage clickMergeButton() {
		WebElement eleMergeButton = locateElement("xpath", "//a[text()='Merge']");
		click(eleMergeButton);
		return new ViewLeadPage();
	}
	
	public MergeLeadsPage clickAlert() {
		acceptAlert();
		return this;
	}
}
