package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	
	//Created for Edit
	
	public FindLeadsPage typeFirstName(String fName) {
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, fName);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsbutton() throws InterruptedException {
		WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		Thread.sleep(4000);
		return this;
	}
	
	
	public ViewLeadPage clickFirstRow() {
		WebElement eleFirstRow = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		String data =eleFirstRow.getText();
		click(eleFirstRow);
		return new ViewLeadPage();
	}
	
	//Created for Delete
	
	public FindLeadsPage clickPhone() throws InterruptedException {
		WebElement eleclickPhone = locateElement("xpath", "//span[text()='Phone']");
		click(eleclickPhone);
	   return this;
	}
	
	public FindLeadsPage typePhoneNumber(String PhoneNumber) throws InterruptedException {
		WebElement elePhoneNumber = locateElement("xpath", "//input[@name='phoneNumber']");
		type(elePhoneNumber, PhoneNumber);
		return this;
	}
	
		
	public FindLeadsPage typeLeadid() throws InterruptedException {
		WebElement eleLeadid = locateElement("xpath", "(//div[@class='x-form-element']/input)[13]");
		String data =eleLeadid.getText();
		type(eleLeadid, data);
		return this;
	}
	
	public MergeLeadsPage clickRowOne() {
		WebElement eleRowOne = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)");
		click(eleRowOne);
		return new MergeLeadsPage();
	}
	
	public MergeLeadsPage clickRowTwo() {
		WebElement eleRowTwo = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[2]");
		click(eleRowTwo);
		return new MergeLeadsPage();
	}
	
	//Created for Duplicate
	
	public FindLeadsPage clickemail() throws InterruptedException {
		WebElement eleemail = locateElement("xpath", "//span[text()='Email']");
		click(eleemail);
	   return this;
	}
	
	public FindLeadsPage typeEmail(String eMail) throws InterruptedException {
		WebElement eleEmail = locateElement("xpath", "//input[@name ='emailAddress']");
		type(eleEmail, eMail);
		return this;
	}
}
