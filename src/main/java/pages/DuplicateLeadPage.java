package pages;

import org.openqa.selenium.WebElement;

public class DuplicateLeadPage extends ViewLeadPage {
	
public DuplicateLeadPage verifyExactText() {	
		WebElement verifyeditduplicate = locateElement("xpath", "//div[text()='Duplicate Lead']");
		verifyExactText(verifyeditduplicate, "Duplicate Lead");	
		return this;
	}

public ViewLeadPage clickCreateLead() {
	WebElement eleclickCreateLead = locateElement("xpath", "//input[@value='Create Lead']");
	click( eleclickCreateLead);
	return new ViewLeadPage();
}

}
