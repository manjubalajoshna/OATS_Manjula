package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC005_DuplicateLead";
		testCaseDescription ="Duplicate lead";
		category = "SIT";
		author= "Manjula";
		dataSheetName="duplicateTC005";
}
	
	@Test(dataProvider="fetchData")
	public  void duplicateLead(String eMail) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.clickemail()
		.typeEmail(eMail)
		.clickFindLeadsbutton()
		.clickFirstRow()
		.clickDuplicateLead()
		.verifyExactText()
		.clickCreateLead();
		
}
}
