package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_EDitLead";
		testCaseDescription ="Edit CompanyName";
		category = "SIT";
		author= "ManjuSindhu";
		dataSheetName="editTC002";
	}
	@Test(dataProvider="fetchData")
	public  void editLead(String fName, String cName) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.typeFirstName(fName)
		.clickFindLeadsbutton()
		.clickFirstRow()
		.clickEdit()
		.typeCompanyName(cName)
		.clickUpdateCname()
		.verifyExactText();
		
		
		
		
		
		
	}

}
